# Conception Orientée Objet - Java
## 1 - Design Pattern Factory

* L'implémentation de cette application est basé sur la **factory**, un des ``design pattern`` les plus utilisés en **java**.
* La factory est un design pattern qui permet de créer des objets en fonction d’un type de données, en masquant la logique de création au client
* Nous utilisons une ``interface`` commune pour consulter **l’objet** nouvellement créé, une `pizza`